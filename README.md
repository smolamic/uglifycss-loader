# uglifycss-loader for webpack

## Example:
add something like this to your webpack.conf.js
```
module: {
  rules: [{
    test: /\.(css|scss|sass)$/,
    use: [{
      loader: MiniCssExtractPlugin.loader,
    },{
      loader: 'css-loader',
    }, {
      loader: 'uglifycss-loader',
    }, {
      loader: 'sass-loader',
    }],
  }],
}
```

Found a bug? Need a feature? Leave me a ticket
[here](https://gitlab.com/smolamic/uglifycss-loader/issues/service_desk)
