const uglifycss = require('uglifycss');

module.exports = function(content) {
  return uglifycss.processString(content);
};
